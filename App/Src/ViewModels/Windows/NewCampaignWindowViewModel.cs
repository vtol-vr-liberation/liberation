﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Liberation.Common.Campaign;
using Liberation.Common.IO;
using Liberation.IO;

namespace Liberation.ViewModels;


// ViewModel for the new campaign setup window
public sealed class NewCampaignWindowViewModel : ViewModelBase
{
	#region Fields
	// The name of the campaign
	public string CampaignName {
		get => _campaignName;
		set => SetProperty(ref _campaignName, value);
	}
	private string _campaignName = String.Empty;
	
	// The setup status report string
	public string StatusReport {
		get => _statusReport;
		private set => SetProperty(ref _statusReport, value);
	}
	private string _statusReport = String.Empty;

	// If the current status represents an error state
	public bool HasError {
		get => _hasError;
		private set => SetProperty(ref _hasError, value);
	}
	private bool _hasError;
	#endregion // Fields


	// Sets the status text as an error
	public void SetErrorStatus(string status) => (StatusReport, HasError) = (status, true);
	
	// Sets the status text as a non-error
	public void SetStatus(string status) => (StatusReport, HasError) = (status, false);
	
	
	// Perform validation of the model properties
	public bool ValidateProperties()
	{
		// Validate the campaign name
		if (_campaignName.Length == 0) {
			SetErrorStatus("Campaign name cannot be empty");
			return false;
		}
		if (!_campaignName.All(static ch => Char.IsAsciiLetterOrDigit(ch) || ch is '_' or '-' or ' ')) {
			SetErrorStatus("Campaign name must be alphanumeric");
			return false;
		}
		var campaignFile = AppFiles.GetCampaignFilePath(_campaignName);
		if (File.Exists(campaignFile)) {
			SetErrorStatus($"Campaign name '{_campaignName}' already taken");
			return false;
		}
		
		// Success
		SetStatus("");
		return true;
	}
	
	// Creates the new campaign using the current properties
	public async ValueTask<bool> CreateNewCampaign()
	{
		try {
			// Create campaign with current properties
			CampaignData campaign = new(_campaignName);
			
			// Save the blank campaign
			var filePath = AppFiles.GetCampaignFilePath(_campaignName);
			await CampaignFile.SaveCampaignDataAsync(campaign, filePath, default);

			AppLogger.Info($"Saved new campaign file to '{filePath}'");
			return true;
		}
		catch (Exception ex) {
			AppLogger.Error($"Failed to create new campaign: {ex.Message}");
			SetErrorStatus($"Failed: {ex.Message}");
			return false;
		}
	}
}
