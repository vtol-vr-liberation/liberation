﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Liberation.Engine;
using Liberation.IO;

namespace Liberation.ViewModels;


// ViewModel for the setup window that displays at application startup
public sealed class SetupWindowViewModel : ViewModelBase
{
	#region Fields
	// The selected path for the flight logs
	public string FlightLogsPath {
		get => _flightLogsPath;
		set => SetProperty(ref _flightLogsPath, value);
	}
	private string _flightLogsPath = String.Empty;
	
	// The setup status report string
	public string StatusReport {
		get => _statusReport;
		private set => SetProperty(ref _statusReport, value);
	}
	private string _statusReport = String.Empty;

	// If the current status represents an error state
	public bool HasError {
		get => _hasError;
		private set => SetProperty(ref _hasError, value);
	}
	private bool _hasError;
	
	// The currently selected campaign file
	public int SelectedCampaignIndex {
		get => _selectedCampaignIndex;
		set => SetProperty(ref _selectedCampaignIndex, value);
	}
	private int _selectedCampaignIndex;
	
	// The list of available campaign files
	public IReadOnlyList<string> CampaignFiles {
		get => _campaignFiles;
		set => SetProperty(ref _campaignFiles, value);
	}
	private IReadOnlyList<string> _campaignFiles = [];
	#endregion // Fields


	public SetupWindowViewModel()
	{
		// Mostly for testing purposes to speed up getting to the main window
		FlightLogsPath = AppCache.LastFlightLogDirectory;
		
		// Populate the campaign files
		RefreshCampaignList();
		
		// Select the last campaign if possible
		for (var ci = 0; ci < _campaignFiles.Count; ++ci) {
			if (Path.GetFileNameWithoutExtension(_campaignFiles[ci]) != AppCache.LastOpenedCampaign) continue;
			SelectedCampaignIndex = ci;
			break;
		}
	}
	
	
	// Sets the status text as an error
	public void SetErrorStatus(string status) => (StatusReport, HasError) = (status, true);
	
	// Sets the status text as a non-error
	public void SetStatus(string status) => (StatusReport, HasError) = (status, false);
	
	
	// Refresh the list of campaign files
	public void RefreshCampaignList()
	{
		AppFiles.RefreshCampaignFiles();
		CampaignFiles = AppFiles.CampaignFiles
			.Select(static fi => Path.GetFileNameWithoutExtension(fi.Name))
			.ToArray();
	}
	
	// Perform validation of the setup properties
	public bool ValidateProperties()
	{
		// Validate the flight logs
		if (_flightLogsPath.Length == 0) {
			SetErrorStatus("Cannot use empty flight logs path");
			return false;
		}
		try {
			if (!Directory.Exists(_flightLogsPath)) {
				SetErrorStatus("Flight logs path does not exist");
				return false;
			}
		}
		catch {
			SetErrorStatus("Invalid flight logs path");
			return false;
		}
		
		// Validate the campaign
		if (_selectedCampaignIndex < 0 || CampaignFiles.Count == 0) {
			SetErrorStatus("No campaign selected");
			return false;
		}
		if (!AppFiles.CampaignFiles[_selectedCampaignIndex].Exists) {
			SetErrorStatus("Invalid campaign selected");
			return false;
		}
		
		// Success
		SetStatus("");
		return true;
	}
	
	// Setups up the application with the current properties
	public async ValueTask<bool> LoadCampaignAsync()
	{
		SetStatus("Loading campaign...");
		
		// Set the flight log path
		FlightLogWatcher.SetFlightLogDirectory(_flightLogsPath);
		
		// Start the campaign (for now, always select the test campaign)
		var selectedCampaign = AppFiles.CampaignFiles[_selectedCampaignIndex];
		var loadRes = await CampaignEngine.LoadCampaignAsync(selectedCampaign, default);
		
		// Handle campaign start result
		if (loadRes.IsSuccess) {
			SetStatus("Campaign loaded");
			AppCache.LastFlightLogDirectory = _flightLogsPath;
			AppCache.LastOpenedCampaign = Path.GetFileNameWithoutExtension(selectedCampaign.Name);
			return true;
		}
		SetErrorStatus($"Load failed: {loadRes.Message}");
		return false;
	}
}
