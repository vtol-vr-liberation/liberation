﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Linq;
using Liberation.Common.Campaign;
using Liberation.Engine;

namespace Liberation.ViewModels;


// ViewModel for the main window single mission summary panel
public sealed class MissionSummaryViewModel : ViewModelBase
{
	#region Fields
	// The mission event log (one event per newline)
	public string EventLog {
		get => _eventLog;
		set => SetProperty(ref _eventLog, value);
	}
	private string _eventLog = String.Empty;
	#endregion // Fields


	public MissionSummaryViewModel()
	{
		// Subscribe to a mission being added to the campaign engine
		CampaignEngine.OnMissionAdded += mission => 
			EventLog = String.Join('\n', mission.EventLog.Select(static evt => ((MissionEvent.DummyEvent)evt).Raw));
	}
}
