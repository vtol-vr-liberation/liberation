﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;

namespace Liberation.ViewModels;


// ViewModel for the main window status bar
public sealed class StatusBarViewModel : ViewModelBase
{
	
}
