/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using Liberation.IO;
using Liberation.Views;

namespace Liberation;


public partial class App : Application
{
    public override void Initialize() => AvaloniaXamlLoader.Load(this);

    public override void OnFrameworkInitializationCompleted()
    {
        if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop) {
			// Handle the application cache
			AppCache.LoadCache();
			desktop.Exit += static (_, _) => AppCache.SaveCache();
			
			// Start with setup window
			desktop.MainWindow = new SetupWindow();
			
			AppLogger.Info("Started application with desktop lifetime");
		}

        base.OnFrameworkInitializationCompleted();
    }
}
