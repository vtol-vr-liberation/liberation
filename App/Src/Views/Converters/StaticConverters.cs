﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using Avalonia.Data.Converters;
using Avalonia.Media;

namespace Liberation.Views;


// Contains custom value converters for application views
public static class StaticConverters
{
	// Resources used by converters
	public static readonly IBrush ErrorColorBrush = new SolidColorBrush(Colors.Red);
	public static readonly IBrush NonErrorColorBrush = new SolidColorBrush(Colors.Green);
	
	// Converts an error flag boolean into a brush (red for error, green for not error)
	public static FuncValueConverter<bool?, IBrush> ErrorFlagColorConverter { get; } =
		new(static flag => flag is true ? ErrorColorBrush : NonErrorColorBrush);
}
