﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Liberation.ViewModels;

namespace Liberation.Views;


// Panel for displaying the summary of a specific mission
public partial class MissionSummaryPanel : UserControl
{
	public MissionSummaryViewModel ViewModel => (MissionSummaryViewModel)DataContext!;
	
	public MissionSummaryPanel() => InitializeComponent();
}

