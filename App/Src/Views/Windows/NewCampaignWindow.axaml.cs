﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using Avalonia;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Markup.Xaml;
using Avalonia.Threading;
using Liberation.ViewModels;

namespace Liberation.Views;


// Window for setting up a new campaign
public partial class NewCampaignWindow : Window
{
	public NewCampaignWindowViewModel ViewModel => (NewCampaignWindowViewModel)DataContext!;
	
	public NewCampaignWindow() => InitializeComponent();

	
	// Cancel button
	private async void CancelButton_Clicked(object? sender, RoutedEventArgs e) => 
		await Dispatcher.UIThread.InvokeAsync(Close);

	// Accept button
	private async void AcceptButton_Clicked(object? sender, RoutedEventArgs e)
	{
		// Validate
		if (!ViewModel.ValidateProperties()) return;
		
		// Create campaign
		if (!await ViewModel.CreateNewCampaign()) return;
		
		// Close window
		await Dispatcher.UIThread.InvokeAsync(Close);
	}
}

