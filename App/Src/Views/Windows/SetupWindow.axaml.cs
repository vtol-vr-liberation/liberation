﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Interactivity;
using Avalonia.Threading;
using Liberation.ViewModels;

namespace Liberation.Views;


// Window for configuring the campaign paths at startup
public partial class SetupWindow : Window
{
	public SetupWindowViewModel ViewModel => (SetupWindowViewModel)DataContext!;

	public SetupWindow() => InitializeComponent();

	
	// Browse... for flight log directory
	private async void FlightLogBrowseButton_Clicked(object? sender, RoutedEventArgs e)
	{
		// Open the file dialog
		var dirList = await StorageProvider.OpenFolderPickerAsync(new() {
			Title = "Select Flight Logs Directory",
			AllowMultiple = false
		});
		
		// Update fields
		if (dirList.Count >= 1) ViewModel.FlightLogsPath = dirList[0].Path.LocalPath;
	}

	// New... for campaign dropdown
	private async void NewCampaignButton_Clicked(object? sender, RoutedEventArgs e)
	{
		// Open the new campaign modal
		var newCampaignWindow = new NewCampaignWindow();
		await newCampaignWindow.ShowDialog(this);
		
		// Refresh the campaign file list to pick up the potential new one
		ViewModel.RefreshCampaignList();
	}

	// Accept for launch the application
	private async void AcceptButton_Clicked(object? sender, RoutedEventArgs e)
	{
		// Validate the parameters
		if (!ViewModel.ValidateProperties()) return;

		// Setup and load the campaign 
		if (!await ViewModel.LoadCampaignAsync()) return;
		
		// Switch to the main application window
		await Dispatcher.UIThread.InvokeAsync(() => {
			var desktop = (IClassicDesktopStyleApplicationLifetime)Application.Current!.ApplicationLifetime!;
			desktop.MainWindow = new MainWindow();
			desktop.MainWindow.Show();
			Close();
		});
	}
}

