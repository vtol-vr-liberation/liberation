/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using Avalonia.Controls;
using Liberation.ViewModels;

namespace Liberation.Views;


// Main application window
public partial class MainWindow : Window
{
	public MainWindowViewModel ViewModel => (MainWindowViewModel)DataContext!;

	public MainWindow() => InitializeComponent();
}
