﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using Avalonia.Controls;
using Liberation.ViewModels;

namespace Liberation.Views;


// Main application status bar
public partial class StatusBar : UserControl
{
	public StatusBarViewModel ViewModel => (StatusBarViewModel)DataContext!;
	
	public StatusBar() => InitializeComponent();
}

