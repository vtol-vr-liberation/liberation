﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Liberation.ViewModels;

namespace Liberation.Views;


// Panel for displaying an overall summary of the campaign
public partial class CampaignSummaryPanel : UserControl
{
	public CampaignSummaryViewModel ViewModel => (CampaignSummaryViewModel)DataContext!;
    
	public CampaignSummaryPanel() => InitializeComponent();
}

