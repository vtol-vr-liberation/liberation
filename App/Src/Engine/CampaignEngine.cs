﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Liberation.Common.Campaign;
using Liberation.Common.IO;
using Liberation.IO;

namespace Liberation.Engine;


// Manages the actively selected campaign, including performing updates and mission generation
public static class CampaignEngine
{
	#region Fields
	// The currently loaded campaign
	public static CampaignData? Current { get; private set; }
	
	// Events
	public static event Action<MissionData>? OnMissionAdded;
	#endregion // Fields


	// Handles the creation of a new flight log, ingesting and processing the mission it represents
	private static async ValueTask HandleNewFlightLog(FileInfo flightLog)
	{
		if (Current is null) return;
		
		// Wait a short time to allow the file to be written and closed
		// TODO: This is fragile, handle this better
		await Task.Delay(1000);
		
		// Testing only - read the flight log into a new mission
		var flightEvents = await FlightLogReader.ReadFlightLogAsync(flightLog.FullName, default);
		OnMissionAdded?.Invoke(new(flightEvents));
	}
	
	
	// Loads the given campaign
	public static async ValueTask<LoadResult> LoadCampaignAsync(FileInfo campaignFile, CancellationToken token)
	{
		var first = Current is null;
		
		// Try to load the campaign
		try {
			Current = await CampaignFile.LoadCampaignDataAsync(campaignFile.FullName, token);
			AppLogger.Info($"Loaded campaign '{Current.Name}' from: {campaignFile.FullName}");
		}
		catch (Exception ex) {
			AppLogger.Error($"Failed to load campaign '{campaignFile}', reason: {ex.Message}");
			return new(false, ex.Message);
		}
		
		// For the first load, also handle general setup
		if (first) {
			FlightLogWatcher.OnNewFlightLog += static async file => await HandleNewFlightLog(file);
		}
		
		// Report success
		return new(true, null);
	}
	
	
	// Communicates a campaign load result
	public sealed record LoadResult(bool IsSuccess, string? Message);
}
