﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.IO;
using System.Text;
using System.Threading;
using Liberation.IO;

namespace Liberation;


// Handles message logging for the application
public static class AppLogger
{
	// Logger message levels
	public enum LogLevel { Debug, Info, Warn, Error }
	
	// Delegate for handling posted log messages
	public delegate void LogMessageHandler(LogLevel level, DateTime timeStamp, string message);
	
	
	#region Fields
	// Log file handle
	private static readonly StreamWriter? _LogFile;
	
	// Sync root for log message handling
	private static readonly object _SyncRoot = new();
	
	// Thread-local message builder
	private static readonly ThreadLocal<StringBuilder> _MessageBuilder = new(() => new(256), false); 
		
	// Callback for log messages
	public static event LogMessageHandler? OnLogMessage;
	#endregion // Fields
	
	
	// Posts a DEBUG level log message
	public static void Debug(string message) => PostLogMessage(LogLevel.Debug, message);
	
	// Posts an INFO level log message
	public static void Info(string message) => PostLogMessage(LogLevel.Info, message);
	
	// Posts a WARN level log message
	public static void Warn(string message) => PostLogMessage(LogLevel.Warn, message);
	
	// Posts an ERROR level log message
	public static void Error(string message) => PostLogMessage(LogLevel.Error, message);

	
	// Called to write a log message
	private static void PostLogMessage(LogLevel level, string message)
	{
		lock (_SyncRoot) {
			var now = DateTime.Now;
			
			// Write to the log file if open
			if (_LogFile is not null) {
				var sb = _MessageBuilder.Value!;
				sb.Clear();
				
				// Level tag
				sb.Append('[').Append(level switch {
					LogLevel.Debug => 'D',
					LogLevel.Info  => 'I',
					LogLevel.Warn  => 'W',
					LogLevel.Error => 'E',
					_              => '?'
				}).Append(']');
				
				// Timestamp
				sb.Append('[').Append(now.ToString("HH:mm:ss.ff")).Append(']');
				
				// Message
				sb.Append("  ").Append(message);
				
				_LogFile.WriteLine(sb.ToString());
			}
			
			// Inform about the message
			OnLogMessage?.Invoke(level, now, message);
		}
	}
	

	static AppLogger()
	{
		// Try to open the log file
		try {
			var fileName = $"Liberation_{DateTime.Now:dd-MM-yy_HH-mm-ss}.log";
			var logPath = Path.Combine(AppFiles.LogsDirectory, fileName);
			_LogFile = new(File.Open(logPath, FileMode.Create, FileAccess.Write, FileShare.Read));
		}
		catch {
			_LogFile = null;
		}
		
		// Close log file at end
		AppDomain.CurrentDomain.ProcessExit += static (_, _) => _LogFile?.Dispose();
	}
}
