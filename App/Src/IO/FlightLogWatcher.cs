﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.IO;

namespace Liberation.IO;


// Watches the flight logs directory to listen for new flight logs being generated 
public static class FlightLogWatcher
{
	// Callback for new flight logs
	public delegate void NewFlightLogCallback(FileInfo logFile);
	
	
	#region Fields
	// Raised when a new flight log file is generated
	public static event NewFlightLogCallback? OnNewFlightLog;
	
	// The current filesystem watcher
	private static FileSystemWatcher? _LogWatcher;
	#endregion // Fields


	// Updates the flight log directory
	public static void SetFlightLogDirectory(string logDir)
	{
		// Close the old watcher
		_LogWatcher?.Dispose();
		
		// Check the new directory
		if (!Directory.Exists(logDir)) throw new ArgumentException($"Flight log directory '{logDir}' does not exist");
		
		// Create the new watcher
		_LogWatcher = new(logDir, "*.txt");
		_LogWatcher.IncludeSubdirectories = false;
		_LogWatcher.EnableRaisingEvents = true;
		_LogWatcher.Created += static (_, args) => OnWatcherFileCreated(args);
		
		AppLogger.Info($"Changed flight log directory to '{logDir}'");
	}
	
	
	// Watcher callback for handling file create events
	private static void OnWatcherFileCreated(FileSystemEventArgs args)
	{
		AppLogger.Info($"Flight log watcher discovered new file: '{args.Name}'");
        
		// Some validation may need to be done to ensure it is a flight log, for now just assume blindly
		OnNewFlightLog?.Invoke(new(args.FullPath));
	}


	// Static ctor
	static FlightLogWatcher()
	{
		// Final cleanup callback
		AppDomain.CurrentDomain.ProcessExit += static (_, _) => cleanupWatcher();

		return;
		
		// Cleanups the watcher at app exit
		static void cleanupWatcher() => _LogWatcher?.Dispose();
	}
}
