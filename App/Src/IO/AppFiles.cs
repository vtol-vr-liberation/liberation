﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Liberation.Common.IO;

namespace Liberation.IO;


// Contains fields and utilities for working with application files
public static class AppFiles
{
	// The search patterns for campaign files
	private const string CAMPAIGN_SEARCH_PATTERN = "*." + CampaignFile.FILE_EXT;
	
	// The base directory for the application ("VTOL VR Liberation" in documents)
	public static readonly string BaseDirectory;
	// Save directory for the campaign files
	public static readonly string CampaignSaveDirectory;
	// App data directory
	public static readonly string DataDirectory;
	// App logs directory
	public static readonly string LogsDirectory;
	// The path to the app cache file
	public static readonly string CacheFilePath;
	
	// Enumeration of all *possible* campaign files found in the app directory at startup
	public static IReadOnlyList<FileInfo> CampaignFiles { get; private set; }
	
	
	// Gets the campaign file path for the given campaign name
	public static string GetCampaignFilePath(string campaignName) =>
		Path.Combine(CampaignSaveDirectory, $"{campaignName}.{CampaignFile.FILE_EXT}");
	
	
	// Refreshes the list of campaign files in the campaign save folder
	public static void RefreshCampaignFiles() => CampaignFiles = Directory
		.EnumerateFiles(CampaignSaveDirectory, CAMPAIGN_SEARCH_PATTERN, SearchOption.TopDirectoryOnly)
		.Select(static s => new FileInfo(s))
		.ToArray();


	static AppFiles()
	{
		// Set the paths
		var documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
		BaseDirectory = Path.Combine(documents, "VTOL VR Liberation");
		CampaignSaveDirectory = Path.Combine(BaseDirectory, "Campaigns");
		DataDirectory = Path.Combine(BaseDirectory, "Data");
		LogsDirectory = Path.Combine(BaseDirectory, "Logs");
		CacheFilePath = Path.Combine(BaseDirectory, "Engine.dat");
		
		// Ensure the app directories exist
		if (!Directory.Exists(BaseDirectory)) Directory.CreateDirectory(BaseDirectory);
		if (!Directory.Exists(CampaignSaveDirectory)) Directory.CreateDirectory(CampaignSaveDirectory);
		if (!Directory.Exists(DataDirectory)) Directory.CreateDirectory(DataDirectory);
		if (!Directory.Exists(LogsDirectory)) Directory.CreateDirectory(LogsDirectory);
		
		// Enumerate the existing campaign files (only matches by pattern, the files are not validated)
		CampaignFiles = Array.Empty<FileInfo>();
		RefreshCampaignFiles();
	}
}
