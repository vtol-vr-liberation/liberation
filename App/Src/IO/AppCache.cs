﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Liberation.Common.IO;

namespace Liberation.IO;


// Saving/loading and property queries for the application cache file
public static class AppCache
{
	// Callback for cache property changes
	public delegate void PropertyChangedCallback(string name, object? newValue);


	#region Fields
	// Event for property changes
	public static event PropertyChangedCallback? OnPropertyChanged;
	
	// The last used flight log directory
	public static string LastFlightLogDirectory {
		get => _LastFlightLogDirectory;
		set {
			_LastFlightLogDirectory = value;
			OnPropertyChanged?.Invoke(nameof(LastFlightLogDirectory), _LastFlightLogDirectory);
		}
	}

	private static string _LastFlightLogDirectory = String.Empty;
	
	// The name of the last opened campaign
	public static string LastOpenedCampaign {
		get => _LastOpenedCampaign;
		set {
			_LastOpenedCampaign = value;
			OnPropertyChanged?.Invoke(nameof(LastOpenedCampaign), _LastOpenedCampaign);
		}
	}
	private static string _LastOpenedCampaign = String.Empty;
	#endregion // Fields


	// Loads the cache properties from the on-disk cache file (returns true on loaded, false on load failure)
	public static bool LoadCache()
	{
		try {
			// Load cache file json
			using var fileStream = File.Open(AppFiles.CacheFilePath, FileMode.Open, FileAccess.Read);
			using var document = JsonDocument.Parse(fileStream, JsonHelper.DefaultReaderOptions);
			var json = document.RootElement;

			// Read values
			JsonElement elem;
			LastFlightLogDirectory = json.TryGetProperty(nameof(LastFlightLogDirectory), out elem)
				? elem.GetString() ?? ""
				: "";
			LastOpenedCampaign = json.TryGetProperty(nameof(LastOpenedCampaign), out elem)
				? elem.GetString() ?? ""
				: "";
			
			// Load success
			AppLogger.Info("Loaded app cache");
			return true;
		}
		catch {
			AppLogger.Error("Failed to read app cache file, defaults will be used");
			LastFlightLogDirectory = String.Empty;
			LastOpenedCampaign = String.Empty;
			return false;
		}
	}
	
	// Saves the cache properties to the on-disk cache file (returns true on saved, false on save failure)
	public static bool SaveCache()
	{
		try {
			// Open file and json writer
			using var fileStream = File.Open(AppFiles.CacheFilePath, FileMode.Create, FileAccess.Write);
			using var json = new Utf8JsonWriter(fileStream, JsonHelper.DefaultWriterOptions);

			// Open file
			json.WriteStartObject();
			
			// Write properties
			json.WriteString(nameof(LastFlightLogDirectory), LastFlightLogDirectory);
			json.WriteString(nameof(LastOpenedCampaign), LastOpenedCampaign);
			
			// Close file and return
			json.WriteEndObject();
			AppLogger.Info("Saved app cache");
			return true;
		}
		catch (Exception ex) {
			AppLogger.Error($"Failed to save app cache file, reason: {ex.Message}");
			try { File.Delete(AppFiles.CacheFilePath); } catch { }
			return false;
		}
	}
}
