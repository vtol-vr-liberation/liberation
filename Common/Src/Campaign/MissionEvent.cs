﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;

namespace Liberation.Common.Campaign;


/// <summary>Base type for all mission event descriptions from a VTOL flight log.</summary>
public abstract record MissionEvent
{
	/// <summary>Temporary event type for initial development.</summary>
	/// <param name="Raw">The raw text of the event from the flight log.</param>
	public sealed record DummyEvent(string Raw) : MissionEvent;
}
