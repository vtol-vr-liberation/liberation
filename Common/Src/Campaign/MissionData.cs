﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;

namespace Liberation.Common.Campaign;


/// <summary>In-memory data and state for a specific completed campaign mission.</summary>
public sealed class MissionData(IReadOnlyList<MissionEvent> eventLog)
{
	#region Fields
	/// <summary>The chronologically ordered events of the mission.</summary>
	public readonly IReadOnlyList<MissionEvent> EventLog = eventLog;
	#endregion // Fields
}
