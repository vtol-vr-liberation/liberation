﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;

namespace Liberation.Common.Campaign;


/// <summary>In-memory data and state for a specific campaign.</summary>
public sealed class CampaignData
{
	#region Fields
	/// <summary>The name of the campaign.</summary>
	public readonly string Name;
	#endregion // Fields


	public CampaignData(string name)
	{
		Name = name;
	}
}
