﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Text.Json;

namespace Liberation.Common.IO;


/// <summary>Utilities for working the Json values.</summary>
public static class JsonHelper
{
	/// <summary>Default options for reading Json documents.</summary>
	public static readonly JsonDocumentOptions DefaultReaderOptions = new() {
		AllowTrailingCommas = true, CommentHandling = JsonCommentHandling.Skip
	};
	
	/// <summary>Default options for writing Json documents</summary>
	public static readonly JsonWriterOptions DefaultWriterOptions = new() {
		Encoder = null, Indented = true, SkipValidation = false
	};
}
