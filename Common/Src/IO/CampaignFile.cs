﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.IO;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Liberation.Common.Campaign;

namespace Liberation.Common.IO;


/// <summary>Provides functionality for reading and writing Liberation campaign data files.</summary>
public static class CampaignFile
{
	/// <summary>The default extension for campaign data files, without the leading dot (.).</summary>
	public const string FILE_EXT = "vvrlcampaign";
	
	
	/// <summary>Async loads the campaign data from a file. Throws on load failure.</summary>
	/// <param name="filePath">The file to load.</param>
	/// <param name="token">The task cancellation token.</param>
	/// <returns>The loaded campaign data.</returns>
	public static async Task<CampaignData> LoadCampaignDataAsync(string filePath, CancellationToken token)
	{
		// Check file exists
		if (!File.Exists(filePath)) throw new IOException($"The campaign file '{filePath}' does not exist");
		
		// Read the file into a Json document
		await using var fileStream = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.Read);
		using var json = await JsonDocument.ParseAsync(fileStream, JsonHelper.DefaultReaderOptions, token);
		
		// TODO: Actually perform loading
		
		// Return the campaign data
		return new(Path.GetFileNameWithoutExtension(filePath));
	}


	/// <summary>Async saves the campaign data to a file. Throws on save failure.</summary>
	/// <param name="data">The campaign data to save.</param>
	/// <param name="filePath">The path to save the campaign data to.</param>
	/// <param name="token">The task cancellation token.</param>
	public static async ValueTask SaveCampaignDataAsync(CampaignData data, string filePath, CancellationToken token)
	{
		// Open the file for writing
		await using var fileStream = File.Open(filePath, FileMode.Create, FileAccess.Write, FileShare.None);
		await using var json = new Utf8JsonWriter(fileStream, JsonHelper.DefaultWriterOptions);
		token.ThrowIfCancellationRequested();
		
		// TODO: Actually write the data
		json.WriteStartObject();
		json.WriteEndObject();
	}
}
