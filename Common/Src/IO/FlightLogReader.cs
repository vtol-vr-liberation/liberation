﻿/*
 * MIT License - Copyright (c) VTOL VR Liberation Contributors 2024
 * This file is subject to the terms and conditions of the MIT License, the text of which can be found in the 'LICENSE'
 * file at the root of this repository, or online at <https://opensource.org/licenses/MIT>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Liberation.Common.Campaign;

namespace Liberation.Common.IO;


/// <summary>Provides functionality for reading in VTOL VR generated flight logs.</summary>
public static class FlightLogReader
{
	/// <summary>Async loads the flight log file into a list of events. Throws on load failure.</summary>
	/// <param name="filePath">The flight log file to load.</param>
	/// <param name="token">The task cancellation token.</param>
	/// <returns>The list of events from the flight log in chronological order.</returns>
	public static async Task<List<MissionEvent>> ReadFlightLogAsync(string filePath, CancellationToken token)
	{
		// Check file
		if (!File.Exists(filePath)) throw new IOException($"The flight log file '{filePath}' does not exist");
		
		// Read the raw lines
		var lines = await File.ReadAllLinesAsync(filePath, token);
		var events = new List<MissionEvent>(lines.Length);
		
		// TODO: Process the lines into actual useful events
		for (uint li = 0; li < lines.Length; ++li) {
			events.Add(new MissionEvent.DummyEvent(lines[li]));
		}
		
		// Return the events
		return events;
	}
}
