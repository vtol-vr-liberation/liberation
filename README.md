# Liberation

DCS Liberation brought to VTOL VR.

## Authors

- Sean Moss (Gitlab: [@mosssk](https://gitlab.com/mosssk))
- Matt Foutch (*TODO*)

## License

All code in the project is licensed under the [MIT License](./LICENSE.md).
